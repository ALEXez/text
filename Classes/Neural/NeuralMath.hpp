#ifndef TEXT_NEURAL_MATH_HPP
#define TEXT_NEURAL_MATH_HPP

#include <cmath>

double sigmoid(double x) {
    return 1.0 / (1.0 + exp(-x));
}

double sigmoidDerivative(double x) {
    return sigmoid(x) * (1.0 - sigmoid(x));
}

double getInitialWeight() {
    return static_cast<double>(rand()) / static_cast<double>(RAND_MAX) - 0.5;
}

#endif //TEXT_NEURAL_MATH_HPP
