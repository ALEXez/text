#ifndef TEXT_LAYER_HPP
#define TEXT_LAYER_HPP

#include <vector>
#include <memory>
#include <Neuron.hpp>

class Neuron;

class Layer {
public:
    Layer(unsigned neuronsAmount);
    virtual ~Layer();
    unsigned getSize() const;
    void updateWeights();
    Neuron &operator[](unsigned index);
    const Neuron &operator[](unsigned idx) const;

    virtual void update(const Layer &previousLayer) = 0;

protected:
    std::vector<Neuron> _neurons;
};

#endif //TEXT_LAYER_HPP
