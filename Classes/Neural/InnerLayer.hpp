#ifndef TEXT_INNERLAYER_HPP
#define TEXT_INNERLAYER_HPP

#include <Layer.hpp>

class InnerLayer : public Layer {
public:
    InnerLayer(unsigned neuronsAmount, unsigned previousLayerNeuronsAmount);
    virtual ~InnerLayer();
    virtual void update(const Layer &previousLayer) override;
    void calculateWeightsDelta(const Layer &previousLayer, const Layer &nextLayer, double learningSpeed);

private:
};

#endif //TEXT_INNERLAYER_HPP
