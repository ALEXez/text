#ifndef TEXT_INPUTLAYER_HPP
#define TEXT_INPUTLAYER_HPP

#include <Layer.hpp>
#include <Defines.hpp>

class InputLayer : public Layer {
public:
    InputLayer(unsigned neuronsAmount);
    InputLayer(const InputLayer &) = delete;
    InputLayer &operator=(const InputLayer &) = delete;
    virtual ~InputLayer();
    virtual void update(const Layer &previousLayer) override;
    void setValues(const std::array<double, LETTER_BLOCKS_NUMBER> &initialValues);

private:
};

#endif //TEXT_INPUTLAYER_HPP
