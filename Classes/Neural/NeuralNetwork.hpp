#ifndef TEXT_NEURAL_NETWORK_HPP
#define TEXT_NEURAL_NETWORK_HPP

#include <vector>
#include <memory>
#include <Letter.hpp>
#include <InputLayer.hpp>
#include <InnerLayer.hpp>
#include <OutputLayer.hpp>

class NeuralNetwork {
public:
    NeuralNetwork();
    ~NeuralNetwork();
    void initialize(std::vector<unsigned> innerLayersSizes);
    double getResult(const std::array<double, LETTER_BLOCKS_NUMBER> &initialValues) const;
    void train(const std::array<double, LETTER_BLOCKS_NUMBER> &initialValues, unsigned result, double learningSpeed);
    double getMSE(unsigned correctIndex) const;

private:
    std::unique_ptr<InputLayer> _inputLayer;
    std::vector<std::unique_ptr<InnerLayer>> _innerLayers;
    std::unique_ptr<OutputLayer> _outputLayer;
    void updateLayers() const;
};

#endif //TEXT_NEURAL_NETWORK_HPP
