#include "Neuron.hpp"

#include <NeuralMath.hpp>
#include <cassert>

Neuron::Neuron(unsigned index) : _index(index) {
}

Neuron::~Neuron() {

}

void Neuron::updateValue(const Layer &previousLayer) {
    assert(previousLayer.getSize() == _weights.size());
    _inputValue = 0.0;
    for (unsigned i = 0; i < previousLayer.getSize(); ++i) {
        _inputValue += _weights[i] * previousLayer[i].getValue();
    }
    _outputValue = sigmoid(_inputValue);
}

double Neuron::getValue() const {
    return _outputValue;
}

double Neuron::getError() const {
    return _error;
}

double Neuron::getWeight(unsigned index) const {
    assert(index >= 0 && index < _weights.size());
    return _weights[index];
}

void Neuron::setInitialWeights(unsigned weightsAmount) {
    _weights.resize(weightsAmount);
    _weightsDelta.resize(weightsAmount);
    for (double &weight : _weights) {
        weight = getInitialWeight();
    }
}

void Neuron::setValue(double value) {
    _outputValue = value;
}

void Neuron::calculateWeightsDelta(const Layer &previousLayer, double correctValue, double learningSpeed) {
    assert(previousLayer.getSize() == _weightsDelta.size());
    _error = (correctValue - _outputValue) * sigmoidDerivative(_inputValue);
    for (unsigned i = 0; i < previousLayer.getSize(); ++i) {
        const Neuron &neuron = previousLayer[i];
        _weightsDelta[i] = learningSpeed * _error * neuron.getValue();
    }
}

void Neuron::calculateWeightsDelta(const Layer &previousLayer, const Layer &nextLayer, double learningSpeed) {
    assert(previousLayer.getSize() == _weightsDelta.size());
    double error = 0.0;
    for (unsigned i = 0; i < nextLayer.getSize() - 1; ++i) {
        error += nextLayer[i].getError() * nextLayer[i].getWeight(_index);
    }
    _error = error * sigmoidDerivative(_inputValue);
    for (unsigned i = 0; i < previousLayer.getSize(); ++i) {
        _weightsDelta[i] = learningSpeed * _error * previousLayer[i].getValue();
    }
}

void Neuron::updateWeights() {
    for (unsigned i = 0; i < _weights.size(); ++i) {
        _weights[i] += _weightsDelta[i];
    }
}
