#ifndef TEXT_RECT_HPP
#define TEXT_RECT_HPP

struct Rect {
    unsigned top, bottom;
    unsigned left, right;
};

#endif //TEXT_RECT_HPP
