#include "TrainingManager.hpp"
#include <Image.hpp>
#include <iostream>
#include <cassert>

TrainingLetter::TrainingLetter() {

}

TrainingLetter::TrainingLetter(const std::string letterPath) {
    Image image(letterPath);
    Rect rect;
    for (unsigned i = 0; i < image.getHeight(); ++i) {
        auto value = image.getLineHorizontal(i);
        if (value < 1.0) {
            rect.top = i;
            break;
        }
    }
    for (unsigned i = image.getHeight() - 1; i > 0; --i) {
        auto value = image.getLineHorizontal(i);
        if (value < 1.0) {
            rect.bottom = i;
            break;
        }
    }
    for (unsigned i = 0; i < image.getWidth(); ++i) {
        auto value = image.getLineVertical(i);
        if (value < 1.0) {
            rect.left = i;
            break;
        }
    }
    for (unsigned i = image.getWidth() - 1; i > 0; --i) {
        auto value = image.getLineVertical(i);
        if (value < 1.0) {
            rect.right = i;
            break;
        }
    }
//    std::cout << "rect.top = " << rect.top << std::endl;
//    std::cout << "rect.bottom = " << rect.bottom << " " << image.getHeight() << std::endl;
//    std::cout << "rect.left = " << rect.left << std::endl;
//    std::cout << "rect.right = " << rect.right << " " << image.getWidth() << std::endl;

    const auto &pixels = image.getCropImage(rect);
    assert(LETTER_BLOCKS_NUMBER == pixels.size() * pixels[0].size());
    for (unsigned x = 0; x < LETTER_WIDTH; ++x) {
        for (unsigned y = 0; y < LETTER_WIDTH; ++y) {
            _values[x * LETTER_WIDTH + y] = pixels[x][y].value;
        }
    }
}

TrainingLetter::~TrainingLetter() {

}

const std::array<double, LETTER_BLOCKS_NUMBER> &TrainingLetter::getValues() const {
    return _values;
}

TrainingSet::TrainingSet(const std::string &setPath, const std::string &name) : _name(name) {
    unsigned index = 0;
    char letter = 'a';
    for (; letter < 'z'; ++letter, ++index) {
        _letters[index] = TrainingLetter(setPath + letter + ".png");
    }
}

TrainingSet::~TrainingSet() {

}

const TrainingLetter &TrainingSet::getLetter(unsigned index) const {
    return _letters[index];
}

const std::string &TrainingSet::getName() const {
    return _name;
}

TrainingManager &TrainingManager::getInstance() {
    static TrainingManager instance;
    return instance;
}

TrainingManager::TrainingManager() {

}

void TrainingManager::addSet(const std::string &setName) {
    std::string setPath = "sets/" + setName + "/";
    _sets.emplace_back(TrainingSet(setPath, setName));
}

const TrainingSet &TrainingManager::getSet(unsigned index) const {
    return _sets[index];
}

unsigned TrainingManager::getSetsSize() const {
    return _sets.size();
}
