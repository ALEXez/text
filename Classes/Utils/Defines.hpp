#ifndef TEXT_DEFINES_HPP
#define TEXT_DEFINES_HPP

constexpr unsigned SET_SIZE = 26;
constexpr unsigned LETTER_WIDTH = 20;
constexpr unsigned LETTER_BLOCKS_NUMBER = LETTER_WIDTH * LETTER_WIDTH;

#endif //TEXT_DEFINES_HPP
