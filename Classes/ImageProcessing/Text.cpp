#include "Text.hpp"

#include <iostream>
#include <Classes/Utils/ThreadPool.hpp>

Text::Text(const Image &source)
        : _source(source) {

}

Text::~Text() {

}

void Text::process() {
    calculateRect();
    bool inLine = true;
    unsigned start = _rect.top;
    for (unsigned i = _rect.top; i < _rect.bottom; ++i) {
        auto value = _source.getLineHorizontal(i, _rect);
        if (value >= 1.0 && inLine) {
            _lines.push_back(Line(_source, {start, i - 1, _rect.left, _rect.right}));
            inLine = false;
        } else if (value < 1.0 && !inLine) {
            start = i;
            inLine = true;
        }
    }
    _lines.push_back(Line(_source, {start, _rect.bottom, _rect.left, _rect.right}));
//    std::cout << "lines = " << _lines.size() << std::endl;

    for (Line &line : _lines) {
        ThreadPool::getInstance().enqueue([this, &line] {
            line.process();
        });
    }
    ThreadPool::getInstance().wait();
}

const std::vector<Line> &Text::getLines() const {
    return _lines;
}

void Text::calculateRect() {
//    std::cout << "width = " << _source.getWidth() << std::endl;
//    std::cout << "height = " << _source.getHeight() << std::endl;
    for (unsigned i = 0; i < _source.getHeight(); ++i) {
        auto value = _source.getLineHorizontal(i);
        if (value < 1.0) {
            _rect.top = i;
            break;
        }
    }
    for (unsigned i = _source.getHeight() - 1; i > 0; --i) {
        auto value = _source.getLineHorizontal(i);
        if (value < 1.0) {
            _rect.bottom = i;
            break;
        }
    }
    for (unsigned i = 0; i < _source.getWidth(); ++i) {
        auto value = _source.getLineVertical(i);
        if (value < 1.0) {
            _rect.left = i;
            break;
        }
    }
    for (unsigned i = _source.getWidth() - 1; i > 0; --i) {
        auto value = _source.getLineVertical(i);
        if (value < 1.0) {
            _rect.right = i;
            break;
        }
    }
//    std::cout << "rect.top = " << _rect.top << std::endl;
//    std::cout << "rect.bottom = " << _rect.bottom << std::endl;
//    std::cout << "rect.left = " << _rect.left << std::endl;
//    std::cout << "rect.right = " << _rect.right << std::endl;
}
