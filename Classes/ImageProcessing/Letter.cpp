#include "Letter.hpp"

#include <cassert>
#include <iostream>

Letter::Letter(const Image &source, const Rect &rect)
        : _source(source), _rect(rect) {

}

void Letter::process() {
    clipRect();
    const auto &pixels = _source.getCropImage(_rect);
    assert(LETTER_BLOCKS_NUMBER == pixels.size() * pixels[0].size());
    for (unsigned x = 0; x < LETTER_WIDTH; ++x) {
        for (unsigned y = 0; y < LETTER_WIDTH; ++y) {
            _pieces[x * LETTER_WIDTH + y] = pixels[x][y].value;
        }
    }
}

const std::array<double, LETTER_BLOCKS_NUMBER> &Letter::getPieces() const {
    return _pieces;
}

void Letter::clipRect() {
    for (unsigned i = _rect.top; i <= _rect.bottom; ++i) {
        auto value = _source.getLineHorizontal(i, _rect);
        if (value < 1.0) {
            _rect.top = i;
            break;
        }
    }
    for (unsigned i = _rect.bottom; i >= _rect.top; --i) {
        auto value = _source.getLineHorizontal(i, _rect);
        if (value < 1.0) {
            _rect.bottom = i;
            break;
        }
    }
//    std::cout << "rect.top = " << _rect.top << std::endl;
//    std::cout << "rect.bottom = " << _rect.bottom << std::endl;
//    std::cout << "rect.left = " << _rect.left << std::endl;
//    std::cout << "rect.right = " << _rect.right << std::endl;
}
