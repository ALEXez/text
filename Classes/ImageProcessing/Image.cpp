#include "Image.hpp"

#include <CImgWrapper.hpp>
#include <iostream>
#include <cassert>

Image::Image(const std::string &filename, bool isSquareNeeded) : _pixels(CImgWrapper(filename, isSquareNeeded).getPixels()), _filename(filename) { }

Image::~Image() { }

const Pixel &Image::getPixel(unsigned x, unsigned y) const {
    return _pixels[x][y];
}

double Image::getLineHorizontal(unsigned y) const {
    double sum = 0.0;
    for (unsigned x = 0; x < _pixels.size(); ++x) {
        sum += _pixels[x][y].value;
    }
    return sum / _pixels.size();
}

double Image::getLineVertical(unsigned x) const {
    double sum = 0.0;
    for (unsigned y = 0; y < _pixels[x].size(); ++y) {
        sum += _pixels[x][y].value;
    }
    return sum / _pixels[x].size();
}

double Image::getLineHorizontal(unsigned y, const Rect &rect) const {
    assert(y >= rect.top && y <= rect.bottom);
    double sum = 0.0;
    for (unsigned x = rect.left; x <= rect.right; ++x) {
        sum += _pixels[x][y].value;
    }
    return sum / (rect.right - rect.left + 1);
}

double Image::getLineVertical(unsigned x, const Rect &rect) const {
    assert(x >= rect.left && x <= rect.right);
    double sum = 0.0;
    for (unsigned y = rect.top; y <= rect.bottom; ++y) {
        sum += _pixels[x][y].value;
    }
    return sum / (rect.bottom - rect.top + 1);
}

unsigned Image::getWidth() const {
    return _pixels.size();
}

unsigned Image::getHeight() const {
    return _pixels[0].size();
}

std::vector<std::vector<Pixel>> Image::getCropImage(const Rect &rect) const {
    return CImgWrapper(_filename, rect).getPixels();
}
