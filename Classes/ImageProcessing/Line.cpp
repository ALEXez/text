#include "Line.hpp"

#include <iostream>

Line::Line(const Image &source, const Rect &rect)
        : _source(source), _rect(rect) {

}

Line::~Line() {

}

void Line::process() {
    clipRect();
    const std::vector<Rect> &spaces = getSpaces();

    unsigned start = _rect.left;
    for (const auto &space : spaces) {
        _words.emplace_back(Word(_source, {_rect.top, _rect.bottom, start, space.left - 1}));
        start = space.right + 1;
    }
    _words.emplace_back(Word(_source, {_rect.top, _rect.bottom, start, _rect.right}));
//    std::cout << "words = " << _words.size() << std::endl;

    for (Word &word : _words) {
        word.process();
    }
}

const std::vector<Word> &Line::getWords() const {
    return _words;
}

void Line::clipRect() {
    for (unsigned i = _rect.left; i <= _rect.right; ++i) {
        auto value = _source.getLineVertical(i, _rect);
        if (value < 1.0) {
            _rect.left = i;
            break;
        }
    }
    for (unsigned i = _rect.right; i >= _rect.left; --i) {
        auto value = _source.getLineVertical(i, _rect);
        if (value < 1.0) {
            _rect.right = i;
            break;
        }
    }
//    std::cout << "rect.top = " << _rect.top << std::endl;
//    std::cout << "rect.bottom = " << _rect.bottom << std::endl;
//    std::cout << "rect.left = " << _rect.left << std::endl;
//    std::cout << "rect.right = " << _rect.right << std::endl;
}

std::vector<Rect> Line::getSpaces() const {
    std::vector<Rect> spaces;
    std::vector<Rect> realSpaces;
    bool inSpace = false;
    unsigned start = _rect.left;
    for (unsigned i = _rect.left; i <= _rect.right; ++i) {
        auto value = _source.getLineVertical(i, _rect);
        if (value >= 1.0 && !inSpace) {
            start = i;
            inSpace = true;
        } else if (value < 1.0 && inSpace) {
            spaces.emplace_back(Rect{0, 0, start, i - 1});
            inSpace = false;
        }
    }
    double averageSpace = 0.0;
    for (const Rect &space : spaces) {
        averageSpace += space.right - space.left;
    }
    averageSpace /= spaces.size();

    unsigned count = 0;
    for (const Rect &space : spaces) {
        if (space.right - space.left > averageSpace) {
            ++count;
            realSpaces.push_back(space);
        }
    }
//    std::cout << "spaces = " << spaces.size() << ", count = " << realSpaces.size() << ", aver = " << averageSpace << std::endl;
    return realSpaces;
}
