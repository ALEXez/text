#ifndef TEXT_TEXT_HPP
#define TEXT_TEXT_HPP

#include <Line.hpp>
#include <Rect.hpp>

class Text {
public:
    Text(const Image &source);
    ~Text();

    void process();
    const std::vector<Line> &getLines() const;

private:
    void calculateRect();

    const Image &_source;
    Rect _rect;
    std::vector<Line> _lines;
};

#endif //TEXT_TEXT_HPP
