#include "CImgWrapper.hpp"

#include <CImg/CImg.h>
#include <Defines.hpp>
#include <cassert>

using namespace cimg_library;

CImgWrapper::CImgWrapper(const std::string &filename, bool isSquareNeeded) {
    CImg<double> img = CImg<>(filename.c_str());
    if (isSquareNeeded) {
        img.resize(LETTER_WIDTH, LETTER_WIDTH);
    }

    auto size = img.size();
    auto width = img.width();
    auto height = img.height();
    assert(size == width * height);
    for (decltype(size) column = 0; column < width; ++column) {
        _pixels.push_back(std::vector<Pixel>(height));
    }
    for (decltype(size) i = 0; i < size; ++i) {
        _pixels[i % width][i / width].value = img.at(i) / 255;
    }
}

CImgWrapper::CImgWrapper(const std::string &filename, const Rect &rect) {
    CImg<double> img = CImg<>(filename.c_str());
    img.crop(rect.left, rect.top, rect.right - 1, rect.bottom - 1);
    auto size = img.size();
    auto width = img.width();
    auto height = img.height();
    assert(rect.right - rect.left == width && rect.bottom - rect.top == height);

    img.resize(LETTER_WIDTH, LETTER_WIDTH);

    size = img.size();
    width = img.width();
    height = img.height();

    for (decltype(size) column = 0; column < width; ++column) {
        _pixels.push_back(std::vector<Pixel>(height));
    }
    for (decltype(size) i = 0; i < size; ++i) {
        _pixels[i % width][i / width].value = img.at(i) / 255;
    }

}

const std::vector<std::vector<Pixel>> &CImgWrapper::getPixels() {
    return _pixels;
}
