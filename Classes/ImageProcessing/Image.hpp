#ifndef TEXT_IMAGE_HPP
#define TEXT_IMAGE_HPP

#include <Pixel.hpp>
#include <vector>
#include <string>
#include <Rect.hpp>

class Image {
public:
    Image(const std::string &filename, bool isSquareNeeded = false);
    ~Image();
    Image(Image const &) = delete;
    Image &operator=(Image const &) = delete;
    Image(Image &&) = delete;
    Image &operator=(Image &&) = delete;
    const Pixel &getPixel(unsigned x, unsigned y) const;
    double getLineHorizontal(unsigned y) const;
    double getLineVertical(unsigned x) const;
    double getLineHorizontal(unsigned y, const Rect &rect) const;
    double getLineVertical(unsigned x, const Rect &rect) const;
    unsigned getWidth() const;
    unsigned getHeight() const;
    std::vector<std::vector<Pixel>> getCropImage(const Rect &rect) const;

private:
    std::vector<std::vector<Pixel>> _pixels;
    std::string _filename;
};

#endif //TEXT_IMAGE_HPP
