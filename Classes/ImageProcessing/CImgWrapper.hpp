#ifndef TEXT_CIMGWRAPPER_HPP
#define TEXT_CIMGWRAPPER_HPP

#include <Pixel.hpp>
#include <vector>
#include <string>
#include <Rect.hpp>

class CImgWrapper {
public:
    CImgWrapper(const std::string &filename, bool isSquareNeeded);
    CImgWrapper(const std::string &filename, const Rect &rect);
    const std::vector<std::vector<Pixel>> &getPixels();

private:
    std::vector<std::vector<Pixel>> _pixels;
};


#endif //TEXT_CIMGWRAPPER_HPP
