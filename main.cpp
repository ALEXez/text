#include <iostream>
#include <NeuralNetwork.hpp>
#include <TrainingManager.hpp>
#include <deque>
#include <future>
#include <ThreadPool.hpp>
#include <Text.hpp>

//unsigned getRandomIndex() {
//    return rand() % ('z' - 'a');
//}
//
//void updateStatisticInfo(std::deque<unsigned> &statistic, unsigned newResult) {
//    statistic.push_back(newResult);
//    if (statistic.size() >= 500) {
//        statistic.pop_front();
//    }
//}
//
//unsigned getStatisticInfo(const std::deque<unsigned> &statistic) {
//    unsigned result = 0;
//    for (unsigned i = 0; i < statistic.size(); ++i) {
//        result += statistic[i];
//    }
//    return result / statistic.size();
//}
//
//std::pair<unsigned, unsigned> getTeachResult(unsigned hiddenSize) {
//    std::unique_ptr<NeuralNetwork> network = std::make_unique<NeuralNetwork>();
//    network->initialize(std::vector<unsigned>{hiddenSize});
//    unsigned result;
//    std::deque<unsigned> statistic;
//    for (unsigned long long iter = 1;; ++iter) {
//        const auto &dictLetter = TrainingManager::getInstance().getLetter(getRandomIndex());
//        for (unsigned i = 0;; ++i) {
//            network->train(dictLetter.second.getPieces(), TrainingManager::getInstance().getIndex(dictLetter.first), 0);
//            result = network->getResult(dictLetter.second.getPieces());
//            if (result == TrainingManager::getInstance().getIndex(dictLetter.first)) {
//                updateStatisticInfo(statistic, i);
//                break;
//            }
//        }
//        if (iter == 25000) {
//            break;
//        }
//    }
//    return std::pair<unsigned, unsigned>{hiddenSize, getStatisticInfo(statistic)};
//}

int main() {
    srand(time(nullptr));
    constexpr unsigned ITERATION_NUMBER = 10000000;

    const std::vector<std::string> trainingSets{"ubuntu-mono"};

    for (const auto &set : trainingSets) {
        TrainingManager::getInstance().addSet(set);
    }

    std::vector<std::unique_ptr<NeuralNetwork>> networks;
    for (char l = 'a'; l <= 'z'; ++l) {
        std::unique_ptr<NeuralNetwork> &&network = std::make_unique<NeuralNetwork>();
        network->initialize(std::vector<unsigned>{});
        networks.emplace_back(std::move(network));
    }

    std::cout << "====== Training ======" << std::endl;
    for (unsigned setIndex = 0; setIndex < TrainingManager::getInstance().getSetsSize(); ++setIndex) {
        const TrainingSet &trainingSet = TrainingManager::getInstance().getSet(setIndex);
        std::cout << "Set: " << trainingSet.getName() << " |" << std::flush;
        for (unsigned long long iter = 1; iter < ITERATION_NUMBER; ++iter) {
            for (unsigned i = 0; i < SET_SIZE; ++i) {
                ThreadPool::getInstance().enqueue([&networks, i, &trainingSet] {
                    networks[i]->train(trainingSet.getLetter(i).getValues(), i, 1.0);
                });
            }
            ThreadPool::getInstance().wait();
            if (iter % (ITERATION_NUMBER / 26) == 0) {
                std::cout << "." << std::flush;
            }
        }
        std::cout << "| Done" << std::endl;
    }

    std::cout << "====== Text parsing ======" << std::endl;
    const Image &mainImage = Image("text.png");
    Text text = Text(mainImage);
    text.process();

    std::cout << "====== Text recognition ======" << std::endl;
    for (const Line &line : text.getLines()) {
        for (const Word &word : line.getWords()) {
            for (const Letter &letter : word.getLetters()) {
                std::pair<unsigned, double> max = std::make_pair(0, 0.0);
                for (unsigned i = 0; i < SET_SIZE; ++i) {
                    double result = networks[i]->getResult(letter.getPieces());
                    if (result > max.second) {
                        max.first = i;
                        max.second = result;
                    }
                }
                std::cout << static_cast<char>(max.first + 'a');
            }
            std::cout << " ";
        }
        std::cout << "\n";
    }
    std::cout << std::flush;

//
//    for (char g = 'z'; g >= 'a'; --g) {
//        std::pair<char, double> max = std::make_pair(-1, 0.0);
//        for (char l = 'a'; l <= 'z'; ++l) {
//            unsigned neuralIndex = l - 'a';
//            const unsigned letterIndex = TrainingManager::getInstance().getIndex(g);
//            const auto &dictLetter = TrainingManager::getInstance().getLetter(letterIndex);
//
//            double result = networks[neuralIndex]->getResult(dictLetter.second.getPieces());
//            if (result > max.second) {
//                max.first = l;
//                max.second = result;
//            }
//        }
//        std::cout << "Give: " << g << ", receive: " << max.first << std::endl;
//    }

//    std::unique_ptr<NeuralNetwork> network = std::make_unique<NeuralNetwork>();
//    network->initialize(std::vector<unsigned>{});
//    unsigned result;
//    std::deque<unsigned> statistic;
//    for (unsigned long long iter = 1;; ++iter) {
//        double mse = 0.0;
//        unsigned right = 0;
//        for (char l = 'd'; l <= 'z'; ++l) {
//            const unsigned index = TrainingManager::getInstance().getIndex(l);
//            const auto &dictLetter = TrainingManager::getInstance().getLetter(index);
//            const unsigned index2 = getRandomIndex();
//            const auto &dictLetter2 = TrainingManager::getInstance().getLetter(index2);
////            for (unsigned i = 0;; ++i) {
//            const double learningSpeed = 0.7;
//            result = network->getResult(dictLetter.second.getPieces());
//            if (result == TrainingManager::getInstance().getIndex(dictLetter.first)) {
//                ++right;
//            }
////            if (Dictionary::getInstance().getLetter(result).first  != dictLetter.first) {
//            network->train(dictLetter.second.getPieces(), TrainingManager::getInstance().getIndex(dictLetter.first), learningSpeed);
////            network->teach2(dictLetter.second.getPieces(), dictLetter2.second.getPieces(), Dictionary::getInstance().getIndex(dictLetter.first), learningSpeed);
////            }
////            std::cout << "Give: " << dictLetter.first << ", receive: " << Dictionary::getInstance().getLetter(result).first << ", on iteration: " << iter
////                      << std::endl;
//            mse += network->getMSE(TrainingManager::getInstance().getIndex(dictLetter.first));
////                if (result == Dictionary::getInstance().getIndex(dictLetter.first)) {
////                    updateStatisticInfo(statistic, i);
//
////                    break;
////                }
////            }
//            if (l == 'd')
//                break;
//        }
//        std::cout << "MSE: " << mse << " \t" << right << std::endl;
//        if (iter == ITERATION_NUMBER) {
//            break;
//        }
//    }
//    std::cout << getStatisticInfo(statistic) << std::endl;

//    for (char l = 'a'; l <= 'z'; ++l) {
//        const unsigned index = TrainingManager::getInstance().getIndex(l);
//        const auto &dictLetter = TrainingManager::getInstance().getLetter(index);
//        result = network->getResult(dictLetter.second.getPieces());
//        std::cout << "real: " << TrainingManager::getInstance().getLetter(index).first << ", network result: " << TrainingManager::getInstance().getLetter(result).first
//                  << std::endl;
//    }

    return 0;
}
